﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CalculatorPvlt
{
    public partial class frmCalculatorPvlt : Form
    {
        public frmCalculatorPvlt() //main form
        {
            InitializeComponent(); //initializes main form
        }

        double inputOnePvlt; //first textbox input
        double inputTwoPvlt; //second textbox input
        double calculateResultPvlt; //calculates with the two inputs
        int stepColorPvlt = 0; //value of the progress bar
        string setOperatorValuePvlt; //sets operator value for the calculate switch
        string setButtonClickedPvlt; //sets clicked button as a value
        private bool convertSuccesPvlt; //returns converted numbers as succeeded or failed
        private bool convertRootSuccesPvlt; //returns converted root number 1 only as succeeded or failed
        private bool confirmConvertPvlt; //returns confirmation to progress bar

        //////////Buttons Clicked//////////
        private void btnAboutPvlt_Click(object sender, EventArgs e) //clicked on about button
        {
            MessageBox.Show("This application was designed by and made possible thanks to \n \tPatrick Veltrop"); //shows about message in textbox
        }

        private void btnColorPvlt_Click(object sender, EventArgs e) //clicked on color button
        {
            DialogResult dlgResult = colorDialogPvlt.ShowDialog(); //reserves dialog value and brings up color dialog
            if (dlgResult == DialogResult.OK) //checks if user pressed ok and returns it to dlgResult value
            {
                this.BackColor = colorDialogPvlt.Color; //colordialogpvlt is the backcolor of the dialog result, this.backcolor sends it to the main form color
            }
        }

        private void btnClearPvlt_Click(object sender, EventArgs e) //clicked on clear button
        {
            txtbxInputOnePvlt.Text = (""); //clears textbox input one
            txtbxInputTwoPvlt.Text = (""); //clears textbox input two
            this.BackColor = DefaultBackColor; //clears main form color to default
            pgBarPvlt.Value = 0;
            lblMessageTextPvlt.Text = "";
            txtbxOutputPvlt.Text = "";
        }

        private void btnPlusPvlt_Click(object sender, EventArgs e) //clicked on plus button
        {
            setOperatorValuePvlt = "+"; //sends plus operator value to operator switch
            setButtonClickedPvlt = "plusButton"; //sends pressed button input to operator switch
            OperatorSwitchPvlt(); //executes operator switch method
        }

        private void btnMinPvlt_Click(object sender, EventArgs e) //clicked on min button
        {
            setOperatorValuePvlt = "-"; //sends min operator value to operator switch
            setButtonClickedPvlt = "minButton"; //sends pressed button input to operator switch
            OperatorSwitchPvlt(); //executes operator switch method
        }

        private void btnDividePvlt_Click(object sender, EventArgs e) //clicked on clicked on divide button
        {
            setOperatorValuePvlt = "/"; //sends divide operator value to operator switch
            setButtonClickedPvlt = "divideButton"; //sends pressed button input to operator switch
            OperatorSwitchPvlt(); //executes operator switch method
        }

        private void btnMultiplyPvlt_Click(object sender, EventArgs e) //clicked on multiply button
        {
            setOperatorValuePvlt = "*"; //sends multiply operator value to operator switch
            setButtonClickedPvlt = "multiplyButton"; //sends pressed button input to operator switch
            OperatorSwitchPvlt(); //executes operator switch method
        }

        private void btnRootPvlt_Click(object sender, EventArgs e) //clicked on root button
        {
            setOperatorValuePvlt = "√"; //sends root operator value to operator switch
            setButtonClickedPvlt = "rootButton"; //sends pressed button input to operator switch
            OperatorSwitchPvlt(); //executes operator switch method
        }

        private void btnPowPvlt_Click(object sender, EventArgs e) //clicked on power button
        {
            setOperatorValuePvlt = "ⁿ"; //sends power operator value to operator switch
            setButtonClickedPvlt = "powerButton"; //sends pressed button input to operator switch
            OperatorSwitchPvlt(); //executes operator switch method
        }

        private void btnStepPvlt_Click(object sender, EventArgs e) //clicked on step button
        {
            setButtonClickedPvlt = "stepButton"; //sets sent button to separate value so the switch calculates separately
            ProgressIncreasePvlt(); //executes progress increase method on the progress bar
        }

        private void btnLocatePvlt_Click(object sender, EventArgs e) //clicked on locate button
        {
            MessageBox.Show(Application.ExecutablePath); //shows file location of the application
        }

        //////////Operator Switch//////////
        private void OperatorSwitchPvlt() //operator switch method
        {
            switch (setOperatorValuePvlt) //operator switch
            {
                case "+": //operator plus has been received
                    OperatorPlusPvlt(); //executes operator plus method
                    pgBarPvlt.Value = 1; //sets progressbar progress and value
                    lblMessageTextPvlt.Text = (inputOnePvlt) + "+" + (inputTwoPvlt); //prints calculating message on message label
                    break; //ends plus case

                case "-": //operator min has been received
                    OperatorMinPvlt(); //executes operator min method
                    pgBarPvlt.Value = 2; //sets progressbar progress and value
                    lblMessageTextPvlt.Text = (inputOnePvlt) + "-" + (inputTwoPvlt); //prints calculating message on message label
                    break; //ends min case

                case "/": //operator divide has been received
                    OperatorDividePvlt(); //executes operator divide method
                    pgBarPvlt.Value = 3; //sets progressbar progress and value
                    lblMessageTextPvlt.Text = (inputOnePvlt) + "/" + (inputTwoPvlt); //prints calculating message on message label
                    break; //ends divide case

                case "*": //operator multiply has been received
                    OperatorMultiplyPvlt(); //executes operator multiply method
                    pgBarPvlt.Value = 4; //sets progressbar progress and value
                    lblMessageTextPvlt.Text = (inputOnePvlt) + "*" + (inputTwoPvlt); //prints calculating message on message label
                    break; //ends multiply case

                case "√": //operator root has been received
                    OperatorRootPvlt(); //executes operator root method
                    pgBarPvlt.Value = 5; //sets progressbar progress and value
                    lblMessageTextPvlt.Text = (inputOnePvlt) + "√"; //prints calculating message on message label
                    break; //ends root case

                case "ⁿ": //operator power has been received
                    OperatorPowerPvlt(); //executes operator power method
                    pgBarPvlt.Value = 6; //sets progressbar progress and value
                    lblMessageTextPvlt.Text = (inputOnePvlt) + "ⁿ" + (inputTwoPvlt); //prints calculating message on message label
                    break; //ends power case

            }
        }
        /////////////////////////////////////////////////////////////////////////////

        //////////Methods//////////

        private void NumericErrorPvlt() //shows an error if the user doesnt enter numeric values
        {
            convertSuccesPvlt = false; //convert failed since it has text instead of numbers
            MessageBox.Show("Please enter a numeric value."); //a messagebox showing the user to enter a numeric value
            txtbxOutputPvlt.Text = ""; //clears output text
            lblMessageTextPvlt.Text = "..."; //clears calculating label text
        }

        private void ConvertInputsPvlt() //method which converts inputs
        {
            try //tries to execute this code normally
            {
                inputOnePvlt = Convert.ToDouble(txtbxInputOnePvlt.Text); //converts inputone to double variable
                inputTwoPvlt = Convert.ToDouble(txtbxInputTwoPvlt.Text); //converts inputtwo to double variable
            }
            catch //executes this code if the code in the try section fails
            {
                NumericErrorPvlt(); //executes error method
            }
        }

        private void ConvertInputRoot1() //method which converts input for root
        {
            try
            {
                inputOnePvlt = Convert.ToDouble(txtbxInputOnePvlt.Text); //converts input to double variable from textbox inputone
            }
            catch
            {
                NumericErrorPvlt(); //executes error method
            }
        }

        private void PrintResultPvlt() //method which prints calculating result
        {
            txtbxOutputPvlt.Text = Convert.ToString(calculateResultPvlt); //prints calculating result text in the output textbox
        }
        /////////////////////////////////////////////////////////////////////////////

        //////////Progress Bar With the Step Button//////////
        
        private void ProgressIncreasePvlt() //method which changes the progress on the progressbar
        {

            if (pgBarPvlt.Value < 6) //if the value of the progressbar is below 6
            {
                pgBarPvlt.Value++; //increases the progress bar by 1
            }

            else //if the value is any different than above
            {
                pgBarPvlt.Value = 0; //sets the progress bar to 0 if its anything other than above
            }

            switch (pgBarPvlt.Value) //progressbar value switch
            {
                case 0: //if the progressbar value is at 0
                    txtbxOutputPvlt.Text = ""; //clears textbox output
                    lblMessageTextPvlt.Text = ""; //clears label message
                    break; //ends case 0

                case 1: //if the progressbar value is at 1
                    setOperatorValuePvlt = "+"; //sends operator switch value plus
                    OperatorSwitchPvlt(); //executes operator switch method
                    break; //ends case 1

                case 2: //if the progressbar value is at 2
                    setOperatorValuePvlt = "-"; //sends operator switch value min
                    OperatorSwitchPvlt(); //executes operator switch method
                    break; //ends case 2

                case 3: //if the progressbar value is at 3
                    setOperatorValuePvlt = "/"; //sends operator switch value divide
                    OperatorSwitchPvlt(); //executes operator switch method
                    break; //ends case 3

                case 4: //if the progressbar value is at 4
                    setOperatorValuePvlt = "*"; //sends operator switch value multiply
                    OperatorSwitchPvlt(); //executes operator switch method
                    break; //ends case 4

                case 5: //if the progressbar value is at 5
                    setOperatorValuePvlt = "√"; //sends operator switch value root
                    OperatorSwitchPvlt(); //executes operator switch method
                    break; //ends case 5

                case 6: //if the progressbar value is at 6
                    setOperatorValuePvlt = "ⁿ"; //sends operator switch power method
                    OperatorSwitchPvlt(); //executes operator switch method
                    break; //ends case 6

            }
        }

        /////////////////////////////////////////////////////////////////////////////

        //////////Calculator Operators//////////

        private void OperatorPlusPvlt() //plus method
        {
            ConvertInputsPvlt(); //executes converting input text to numbers method
            calculateResultPvlt = inputOnePvlt + inputTwoPvlt; //calculates with the two inputs
            PrintResultPvlt(); //executes printing result method
        }

        private void OperatorMinPvlt()
        {
            ConvertInputsPvlt(); //executes converting input text to numbers method
            calculateResultPvlt = inputOnePvlt - inputTwoPvlt; //calculates with the two inputs
            PrintResultPvlt(); //executes printing result method
        }

        private void OperatorDividePvlt() //divide method
        {
            ConvertInputsPvlt(); //executes converting input text to numbers method
            calculateResultPvlt = inputOnePvlt / inputTwoPvlt; //calculates with the two inputs
            PrintResultPvlt(); //executes printing result method

            if (inputOnePvlt == 0) //if inputone is equal to 0
            {
                MessageBox.Show("Can't divide by 0."); //shows a message to the user
                txtbxOutputPvlt.Text = ""; //clears the output textbox
            }
            else //if inputone is anything else other than 0, continues with
            {
                if (inputTwoPvlt == 0) //if inputtwo is equal to 0
                {
                    MessageBox.Show("Can't divide by 0."); //shows a message to the user
                    txtbxOutputPvlt.Text = ""; //clears the output textbox
                }
            }
        }

        private void OperatorMultiplyPvlt() //multiply method
        {
            ConvertInputsPvlt(); //executes converting input text to numbers method
            calculateResultPvlt = inputOnePvlt * inputTwoPvlt; //calculates with the two inputs
            PrintResultPvlt(); //executes printing result method
        }

        private void OperatorPowerPvlt() //power method
        {
            ConvertInputsPvlt(); //executes converting input text to numbers method
            calculateResultPvlt = Math.Pow(inputOnePvlt, inputTwoPvlt); //calculates with the two inputs
            PrintResultPvlt(); //executes printing result method
        }

        private void OperatorRootPvlt() //root method
        {
            ConvertInputRoot1(); //executes converting input text to numbers method
            calculateResultPvlt = Math.Sqrt(inputOnePvlt); //calculates with the first input
            PrintResultPvlt(); //executes printing result method
            if (inputOnePvlt <= 0) //if input one is equal to or lower than 0
            {
                MessageBox.Show("Can't calculate the root of a value below 0."); //shows a message to the user
            }
        }

        /////////////////////////////////////////////////////////////////////////////
    }
}
