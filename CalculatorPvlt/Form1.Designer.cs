﻿namespace CalculatorPvlt
{
    partial class frmCalculatorPvlt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCalculatorPvlt));
            this.grpbxDataPvlt = new System.Windows.Forms.GroupBox();
            this.txtbxOutputPvlt = new System.Windows.Forms.TextBox();
            this.txtbxInputTwoPvlt = new System.Windows.Forms.TextBox();
            this.txtbxInputOnePvlt = new System.Windows.Forms.TextBox();
            this.lblInputTwoPvlt = new System.Windows.Forms.Label();
            this.lblOutputPvlt = new System.Windows.Forms.Label();
            this.lblInputOnePvlt = new System.Windows.Forms.Label();
            this.grpbxBasicCalPvlt = new System.Windows.Forms.GroupBox();
            this.btnMachtPvlt = new System.Windows.Forms.Button();
            this.btnSqrtPvlt = new System.Windows.Forms.Button();
            this.btnMultiplyPvlt = new System.Windows.Forms.Button();
            this.btnDividePvlt = new System.Windows.Forms.Button();
            this.btnMinusPvlt = new System.Windows.Forms.Button();
            this.btnPlusPvlt = new System.Windows.Forms.Button();
            this.grpbxToolsPvlt = new System.Windows.Forms.GroupBox();
            this.btnStepPvlt = new System.Windows.Forms.Button();
            this.btnClearPvlt = new System.Windows.Forms.Button();
            this.grpbxGenericPvlt = new System.Windows.Forms.GroupBox();
            this.btnColorPvlt = new System.Windows.Forms.Button();
            this.btnAboutPvlt = new System.Windows.Forms.Button();
            this.btnLocatePvlt = new System.Windows.Forms.Button();
            this.lblMessagePvlt = new System.Windows.Forms.Label();
            this.pgBarPvlt = new System.Windows.Forms.ProgressBar();
            this.colorDialogPvlt = new System.Windows.Forms.ColorDialog();
            this.lblMessageTextPvlt = new System.Windows.Forms.Label();
            this.grpbxDataPvlt.SuspendLayout();
            this.grpbxBasicCalPvlt.SuspendLayout();
            this.grpbxToolsPvlt.SuspendLayout();
            this.grpbxGenericPvlt.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpbxDataPvlt
            // 
            this.grpbxDataPvlt.Controls.Add(this.txtbxOutputPvlt);
            this.grpbxDataPvlt.Controls.Add(this.txtbxInputTwoPvlt);
            this.grpbxDataPvlt.Controls.Add(this.txtbxInputOnePvlt);
            this.grpbxDataPvlt.Controls.Add(this.lblInputTwoPvlt);
            this.grpbxDataPvlt.Controls.Add(this.lblOutputPvlt);
            this.grpbxDataPvlt.Controls.Add(this.lblInputOnePvlt);
            this.grpbxDataPvlt.Location = new System.Drawing.Point(13, 13);
            this.grpbxDataPvlt.Name = "grpbxDataPvlt";
            this.grpbxDataPvlt.Size = new System.Drawing.Size(181, 196);
            this.grpbxDataPvlt.TabIndex = 0;
            this.grpbxDataPvlt.TabStop = false;
            this.grpbxDataPvlt.Text = "Data";
            // 
            // txtbxOutputPvlt
            // 
            this.txtbxOutputPvlt.Location = new System.Drawing.Point(63, 90);
            this.txtbxOutputPvlt.Name = "txtbxOutputPvlt";
            this.txtbxOutputPvlt.Size = new System.Drawing.Size(100, 20);
            this.txtbxOutputPvlt.TabIndex = 5;
            // 
            // txtbxInputTwoPvlt
            // 
            this.txtbxInputTwoPvlt.Location = new System.Drawing.Point(63, 64);
            this.txtbxInputTwoPvlt.Name = "txtbxInputTwoPvlt";
            this.txtbxInputTwoPvlt.Size = new System.Drawing.Size(100, 20);
            this.txtbxInputTwoPvlt.TabIndex = 4;
            // 
            // txtbxInputOnePvlt
            // 
            this.txtbxInputOnePvlt.Location = new System.Drawing.Point(63, 38);
            this.txtbxInputOnePvlt.Name = "txtbxInputOnePvlt";
            this.txtbxInputOnePvlt.Size = new System.Drawing.Size(100, 20);
            this.txtbxInputOnePvlt.TabIndex = 3;
            // 
            // lblInputTwoPvlt
            // 
            this.lblInputTwoPvlt.AutoSize = true;
            this.lblInputTwoPvlt.Location = new System.Drawing.Point(10, 67);
            this.lblInputTwoPvlt.Name = "lblInputTwoPvlt";
            this.lblInputTwoPvlt.Size = new System.Drawing.Size(40, 13);
            this.lblInputTwoPvlt.TabIndex = 2;
            this.lblInputTwoPvlt.Text = "Input 2";
            // 
            // lblOutputPvlt
            // 
            this.lblOutputPvlt.AutoSize = true;
            this.lblOutputPvlt.Location = new System.Drawing.Point(11, 96);
            this.lblOutputPvlt.Name = "lblOutputPvlt";
            this.lblOutputPvlt.Size = new System.Drawing.Size(39, 13);
            this.lblOutputPvlt.TabIndex = 1;
            this.lblOutputPvlt.Text = "Output";
            // 
            // lblInputOnePvlt
            // 
            this.lblInputOnePvlt.AutoSize = true;
            this.lblInputOnePvlt.Location = new System.Drawing.Point(10, 38);
            this.lblInputOnePvlt.Name = "lblInputOnePvlt";
            this.lblInputOnePvlt.Size = new System.Drawing.Size(40, 13);
            this.lblInputOnePvlt.TabIndex = 0;
            this.lblInputOnePvlt.Text = "Input 1";
            // 
            // grpbxBasicCalPvlt
            // 
            this.grpbxBasicCalPvlt.Controls.Add(this.btnMachtPvlt);
            this.grpbxBasicCalPvlt.Controls.Add(this.btnSqrtPvlt);
            this.grpbxBasicCalPvlt.Controls.Add(this.btnMultiplyPvlt);
            this.grpbxBasicCalPvlt.Controls.Add(this.btnDividePvlt);
            this.grpbxBasicCalPvlt.Controls.Add(this.btnMinusPvlt);
            this.grpbxBasicCalPvlt.Controls.Add(this.btnPlusPvlt);
            this.grpbxBasicCalPvlt.Location = new System.Drawing.Point(204, 13);
            this.grpbxBasicCalPvlt.Name = "grpbxBasicCalPvlt";
            this.grpbxBasicCalPvlt.Size = new System.Drawing.Size(231, 196);
            this.grpbxBasicCalPvlt.TabIndex = 1;
            this.grpbxBasicCalPvlt.TabStop = false;
            this.grpbxBasicCalPvlt.Text = "Basic Calculations";
            // 
            // btnMachtPvlt
            // 
            this.btnMachtPvlt.Location = new System.Drawing.Point(157, 96);
            this.btnMachtPvlt.Name = "btnMachtPvlt";
            this.btnMachtPvlt.Size = new System.Drawing.Size(51, 50);
            this.btnMachtPvlt.TabIndex = 5;
            this.btnMachtPvlt.Text = "a^b";
            this.btnMachtPvlt.UseVisualStyleBackColor = true;
            this.btnMachtPvlt.Click += new System.EventHandler(this.btnPowPvlt_Click);
            // 
            // btnSqrtPvlt
            // 
            this.btnSqrtPvlt.Location = new System.Drawing.Point(157, 30);
            this.btnSqrtPvlt.Name = "btnSqrtPvlt";
            this.btnSqrtPvlt.Size = new System.Drawing.Size(51, 50);
            this.btnSqrtPvlt.TabIndex = 4;
            this.btnSqrtPvlt.Text = "sqrt";
            this.btnSqrtPvlt.UseVisualStyleBackColor = true;
            this.btnSqrtPvlt.Click += new System.EventHandler(this.btnRootPvlt_Click);
            // 
            // btnMultiplyPvlt
            // 
            this.btnMultiplyPvlt.Location = new System.Drawing.Point(88, 96);
            this.btnMultiplyPvlt.Name = "btnMultiplyPvlt";
            this.btnMultiplyPvlt.Size = new System.Drawing.Size(51, 50);
            this.btnMultiplyPvlt.TabIndex = 3;
            this.btnMultiplyPvlt.Text = "x";
            this.btnMultiplyPvlt.UseVisualStyleBackColor = true;
            this.btnMultiplyPvlt.Click += new System.EventHandler(this.btnMultiplyPvlt_Click);
            // 
            // btnDividePvlt
            // 
            this.btnDividePvlt.Location = new System.Drawing.Point(88, 30);
            this.btnDividePvlt.Name = "btnDividePvlt";
            this.btnDividePvlt.Size = new System.Drawing.Size(51, 50);
            this.btnDividePvlt.TabIndex = 2;
            this.btnDividePvlt.Text = "/";
            this.btnDividePvlt.UseVisualStyleBackColor = true;
            this.btnDividePvlt.Click += new System.EventHandler(this.btnDividePvlt_Click);
            // 
            // btnMinusPvlt
            // 
            this.btnMinusPvlt.Location = new System.Drawing.Point(22, 93);
            this.btnMinusPvlt.Name = "btnMinusPvlt";
            this.btnMinusPvlt.Size = new System.Drawing.Size(51, 50);
            this.btnMinusPvlt.TabIndex = 1;
            this.btnMinusPvlt.Text = "-";
            this.btnMinusPvlt.UseVisualStyleBackColor = true;
            this.btnMinusPvlt.Click += new System.EventHandler(this.btnMinPvlt_Click);
            // 
            // btnPlusPvlt
            // 
            this.btnPlusPvlt.Location = new System.Drawing.Point(22, 30);
            this.btnPlusPvlt.Name = "btnPlusPvlt";
            this.btnPlusPvlt.Size = new System.Drawing.Size(51, 50);
            this.btnPlusPvlt.TabIndex = 0;
            this.btnPlusPvlt.Text = "+";
            this.btnPlusPvlt.UseVisualStyleBackColor = true;
            this.btnPlusPvlt.Click += new System.EventHandler(this.btnPlusPvlt_Click);
            // 
            // grpbxToolsPvlt
            // 
            this.grpbxToolsPvlt.Controls.Add(this.btnStepPvlt);
            this.grpbxToolsPvlt.Controls.Add(this.btnClearPvlt);
            this.grpbxToolsPvlt.Location = new System.Drawing.Point(13, 256);
            this.grpbxToolsPvlt.Name = "grpbxToolsPvlt";
            this.grpbxToolsPvlt.Size = new System.Drawing.Size(163, 124);
            this.grpbxToolsPvlt.TabIndex = 2;
            this.grpbxToolsPvlt.TabStop = false;
            this.grpbxToolsPvlt.Text = "Tools";
            // 
            // btnStepPvlt
            // 
            this.btnStepPvlt.Location = new System.Drawing.Point(14, 74);
            this.btnStepPvlt.Name = "btnStepPvlt";
            this.btnStepPvlt.Size = new System.Drawing.Size(75, 23);
            this.btnStepPvlt.TabIndex = 1;
            this.btnStepPvlt.Text = "Step";
            this.btnStepPvlt.UseVisualStyleBackColor = true;
            this.btnStepPvlt.Click += new System.EventHandler(this.btnStepPvlt_Click);
            // 
            // btnClearPvlt
            // 
            this.btnClearPvlt.Location = new System.Drawing.Point(14, 34);
            this.btnClearPvlt.Name = "btnClearPvlt";
            this.btnClearPvlt.Size = new System.Drawing.Size(75, 23);
            this.btnClearPvlt.TabIndex = 0;
            this.btnClearPvlt.Text = "Clear Input";
            this.btnClearPvlt.UseVisualStyleBackColor = true;
            this.btnClearPvlt.Click += new System.EventHandler(this.btnClearPvlt_Click);
            // 
            // grpbxGenericPvlt
            // 
            this.grpbxGenericPvlt.Controls.Add(this.btnColorPvlt);
            this.grpbxGenericPvlt.Controls.Add(this.btnAboutPvlt);
            this.grpbxGenericPvlt.Controls.Add(this.btnLocatePvlt);
            this.grpbxGenericPvlt.Location = new System.Drawing.Point(204, 256);
            this.grpbxGenericPvlt.Name = "grpbxGenericPvlt";
            this.grpbxGenericPvlt.Size = new System.Drawing.Size(231, 124);
            this.grpbxGenericPvlt.TabIndex = 3;
            this.grpbxGenericPvlt.TabStop = false;
            this.grpbxGenericPvlt.Text = "Generic";
            // 
            // btnColorPvlt
            // 
            this.btnColorPvlt.Location = new System.Drawing.Point(22, 74);
            this.btnColorPvlt.Name = "btnColorPvlt";
            this.btnColorPvlt.Size = new System.Drawing.Size(75, 23);
            this.btnColorPvlt.TabIndex = 4;
            this.btnColorPvlt.Text = "Color";
            this.btnColorPvlt.UseVisualStyleBackColor = true;
            this.btnColorPvlt.Click += new System.EventHandler(this.btnColorPvlt_Click);
            // 
            // btnAboutPvlt
            // 
            this.btnAboutPvlt.Location = new System.Drawing.Point(133, 34);
            this.btnAboutPvlt.Name = "btnAboutPvlt";
            this.btnAboutPvlt.Size = new System.Drawing.Size(75, 23);
            this.btnAboutPvlt.TabIndex = 3;
            this.btnAboutPvlt.Text = "About";
            this.btnAboutPvlt.UseVisualStyleBackColor = true;
            this.btnAboutPvlt.Click += new System.EventHandler(this.btnAboutPvlt_Click);
            // 
            // btnLocatePvlt
            // 
            this.btnLocatePvlt.Location = new System.Drawing.Point(22, 34);
            this.btnLocatePvlt.Name = "btnLocatePvlt";
            this.btnLocatePvlt.Size = new System.Drawing.Size(75, 23);
            this.btnLocatePvlt.TabIndex = 2;
            this.btnLocatePvlt.Text = "Locate";
            this.btnLocatePvlt.UseVisualStyleBackColor = true;
            this.btnLocatePvlt.Click += new System.EventHandler(this.btnLocatePvlt_Click);
            // 
            // lblMessagePvlt
            // 
            this.lblMessagePvlt.AutoSize = true;
            this.lblMessagePvlt.Location = new System.Drawing.Point(10, 227);
            this.lblMessagePvlt.Name = "lblMessagePvlt";
            this.lblMessagePvlt.Size = new System.Drawing.Size(53, 13);
            this.lblMessagePvlt.TabIndex = 6;
            this.lblMessagePvlt.Text = "Message:";
            // 
            // pgBarPvlt
            // 
            this.pgBarPvlt.Location = new System.Drawing.Point(13, 409);
            this.pgBarPvlt.Maximum = 6;
            this.pgBarPvlt.Name = "pgBarPvlt";
            this.pgBarPvlt.Size = new System.Drawing.Size(422, 39);
            this.pgBarPvlt.TabIndex = 7;
            // 
            // lblMessageTextPvlt
            // 
            this.lblMessageTextPvlt.AutoSize = true;
            this.lblMessageTextPvlt.Location = new System.Drawing.Point(69, 227);
            this.lblMessageTextPvlt.Name = "lblMessageTextPvlt";
            this.lblMessageTextPvlt.Size = new System.Drawing.Size(16, 13);
            this.lblMessageTextPvlt.TabIndex = 8;
            this.lblMessageTextPvlt.Text = "...";
            // 
            // frmCalculatorPvlt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(454, 470);
            this.Controls.Add(this.lblMessageTextPvlt);
            this.Controls.Add(this.pgBarPvlt);
            this.Controls.Add(this.lblMessagePvlt);
            this.Controls.Add(this.grpbxGenericPvlt);
            this.Controls.Add(this.grpbxToolsPvlt);
            this.Controls.Add(this.grpbxBasicCalPvlt);
            this.Controls.Add(this.grpbxDataPvlt);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmCalculatorPvlt";
            this.Text = "Calculator";
            this.grpbxDataPvlt.ResumeLayout(false);
            this.grpbxDataPvlt.PerformLayout();
            this.grpbxBasicCalPvlt.ResumeLayout(false);
            this.grpbxToolsPvlt.ResumeLayout(false);
            this.grpbxGenericPvlt.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grpbxDataPvlt;
        private System.Windows.Forms.Label lblInputOnePvlt;
        private System.Windows.Forms.GroupBox grpbxBasicCalPvlt;
        private System.Windows.Forms.GroupBox grpbxToolsPvlt;
        private System.Windows.Forms.GroupBox grpbxGenericPvlt;
        private System.Windows.Forms.TextBox txtbxOutputPvlt;
        private System.Windows.Forms.TextBox txtbxInputTwoPvlt;
        private System.Windows.Forms.TextBox txtbxInputOnePvlt;
        private System.Windows.Forms.Label lblInputTwoPvlt;
        private System.Windows.Forms.Label lblOutputPvlt;
        private System.Windows.Forms.Button btnStepPvlt;
        private System.Windows.Forms.Button btnClearPvlt;
        private System.Windows.Forms.Button btnColorPvlt;
        private System.Windows.Forms.Button btnAboutPvlt;
        private System.Windows.Forms.Button btnLocatePvlt;
        private System.Windows.Forms.Label lblMessagePvlt;
        private System.Windows.Forms.Button btnMachtPvlt;
        private System.Windows.Forms.Button btnSqrtPvlt;
        private System.Windows.Forms.Button btnMultiplyPvlt;
        private System.Windows.Forms.Button btnDividePvlt;
        private System.Windows.Forms.Button btnMinusPvlt;
        private System.Windows.Forms.Button btnPlusPvlt;
        private System.Windows.Forms.ProgressBar pgBarPvlt;
        private System.Windows.Forms.ColorDialog colorDialogPvlt;
        private System.Windows.Forms.Label lblMessageTextPvlt;
    }
}

